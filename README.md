# Floppy Birds Revamp

Flappy Birds clone revamped after finishing the bootcamp.

Instructions: 
    - Press Enter to start
    - Press Space to Jump
    - Press Esc to exit game
    - Press R to restart after you lost (you will).



**Game:**


[![Image from Gyazo](https://i.gyazo.com/bd46a59729a4c4974bfb0ec32cfd2adb.gif)](https://gyazo.com/bd46a59729a4c4974bfb0ec32cfd2adb)
    


