package game_objects;

import floppy.game_objects.Floppy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class FloppyTest {

    private Floppy floppy;

    @Before
    public void setup(){
        floppy = new Floppy(0,0);
    }

    @Test
    public void testMove(){

        floppy.move();

        assertFalse(floppy.jumping);
    }
}
