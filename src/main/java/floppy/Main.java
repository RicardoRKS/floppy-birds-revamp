package floppy;

import floppy.game_objects.Obstacles.TubesPool;
import floppy.keyboard.KeyboardManager;
import floppy.phases.Game;
import floppy.phases.GameOver;
import floppy.phases.Menu;

public class Main {
    public static void main(String[] args) {
        KeyboardManager kbManager = new KeyboardManager();
        kbManager.init();
        Menu intro = new Menu();
        GameOver outro = new GameOver();
        TubesPool pool = new TubesPool(10);
        Game game = new Game(pool);

        while (true){

            kbManager.setHandler(intro);
            intro.run();

            kbManager.setHandler(game);
            game.run();

            kbManager.setHandler(outro);
            outro.run();
        }
    }

}
