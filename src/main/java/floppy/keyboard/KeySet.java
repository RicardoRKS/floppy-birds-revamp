package floppy.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

public enum KeySet {
    ENTER(KeyboardEvent.KEY_ENTER, KeyboardEventType.KEY_PRESSED),
    R(KeyboardEvent.KEY_R, KeyboardEventType.KEY_PRESSED),
    SPACE(KeyboardEvent.KEY_SPACE,KeyboardEventType.KEY_PRESSED),
    ESC(KeyboardEvent.KEY_ESC,KeyboardEventType.KEY_PRESSED);

    public final int KEY_NUM;
    public final KeyboardEventType TYPE;

    KeySet(int KEY_NUM, KeyboardEventType TYPE) {
        this.KEY_NUM = KEY_NUM;
        this.TYPE = TYPE;
    }
}
