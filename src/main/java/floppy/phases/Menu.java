package floppy.phases;

import floppy.keyboard.Handler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Menu implements Handler {
    final private Picture background;
    volatile private boolean idling;

    public Menu() {
        background = new Picture(Game.PADDING, Game.PADDING, "main_menu.png");
        idling = true;
    }

    public void run() {
        background.draw();

        while (idling) {
            //wait
        }
        cleanup();
    }

    private void cleanup() {
        background.delete();
        idling = true;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_ENTER -> idling = false;
            case KeyboardEvent.KEY_Q -> System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
