package floppy.phases;

import floppy.keyboard.Handler;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class GameOver implements Handler {

    final private Picture text;
    final private Picture background;
    volatile private boolean idling;

    public GameOver() {
        background = new Picture(Game.PADDING, Game.PADDING, "blue_sky_background.png");
        text = new Picture(Game.PADDING, Game.PADDING, "game_over_v2.png");
        idling = true;
    }

    public void run(){
        background.draw();
        text.draw();

        while (idling) {
            //wait
        }
        cleanup();
    }

    private void cleanup(){
        text.delete();
        background.delete();
        idling = true;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_R -> idling = false;
            case KeyboardEvent.KEY_Q -> System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
