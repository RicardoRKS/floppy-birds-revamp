package floppy.game_objects.Obstacles;

import floppy.game_objects.Floppy;
import floppy.phases.Game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Tubes {
    private final static int SPEED = 5;

    private final Picture topRepresentation;
    private final Picture botRepresentation;

    public Tubes(TubesInfo info) {
        topRepresentation = new Picture(0, Game.PADDING,info.top);
        botRepresentation = new Picture(0,0,info.bot);
        botRepresentation.translate(0, Game.BOTTOM_LIMIT - botRepresentation.getMaxY());
        toInitialPosition();
    }

    public void show(){
        topRepresentation.draw();
        botRepresentation.draw();
    }

    public void hide(){
        topRepresentation.delete();
        botRepresentation.delete();
    }

    public boolean isTouchingLeft() {
        return botRepresentation.getX() <= Game.PADDING;
    }

    public void toInitialPosition(){
        int xDelta = 0;
        int yDelta = Game.RIGHT_LIMIT-topRepresentation.getMaxX();
        topRepresentation.translate(yDelta, xDelta);
        botRepresentation.translate(yDelta, xDelta);
    }

    public void move() {
        topRepresentation.translate(-SPEED, 0);
        botRepresentation.translate(-SPEED, 0);
    }

    public boolean isTouchingPlayer(Floppy floppy) {
        return playerTouchingTopSide(floppy) || playerTouchingBotSide(floppy);
    }

    private boolean playerTouchingBotSide(Floppy floppy) {
        return botRepresentation.getY() <= floppy.getMaxY()
                && botRepresentation.getX() <= floppy.getMaxX()
                && botRepresentation.getMaxX() >= floppy.getX()
                && botRepresentation.getMaxY() >= floppy.getY();
    }

    private boolean playerTouchingTopSide(Floppy floppy) {
        return topRepresentation.getY() <= floppy.getMaxY()
                && topRepresentation.getX() <= floppy.getMaxX()
                && topRepresentation.getMaxX() >= floppy.getX()
                && topRepresentation.getMaxY() >= floppy.getY();
    }
}
