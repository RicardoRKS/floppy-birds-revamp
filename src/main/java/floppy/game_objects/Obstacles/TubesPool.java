package floppy.game_objects.Obstacles;

import java.util.LinkedList;

public class TubesPool {

    private LinkedList<Tubes> pool;

    public TubesPool(int size) {
        pool = new LinkedList<>();

        for (int i = 0; i < size; i++) {
            pool.add(new Tubes(TubesInfo.getRandom()));
        }
    }

    public Tubes getOne() {
        Tubes o = pool.poll();
        o.show();
        return o;
    }

    public void giveBack(Tubes o) {
        o.hide();
        o.toInitialPosition();
        pool.push(o);
    }
}
