package floppy.game_objects.Obstacles;

public enum  TubesInfo {

    ONE("towers/tower1_top.png", "towers/tower1_bot.png"),
    TWO("towers/tower2_top.png", "towers/tower2_bot.png"),
    THREE("towers/tower3_top.png", "towers/tower3_bot.png");

    public final String top;
    public final String bot;

    TubesInfo(String top, String bot) {
        this.top = top;
        this.bot = bot;
    }

    public static TubesInfo getRandom() {
        return values()[(int)(Math.random() * values().length)];
    }
}
