package floppy.game_objects;


import floppy.phases.Game;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Floppy {

    static final private int SPEED = 3;
    static final private int JUMP_SPEED = SPEED * 2;
    static final private int JUMP_HEIGHT = 50;
    static final private int HITBOX_PADDING = 20;

    final private Picture representation;

    public boolean jumping;
    private int jumpTargetHeight;

    public Floppy(int x, int y) {
        representation = new Picture(x, y, "floppy.png");
        jumping = false;
    }

    public void show() {
        representation.draw();
    }

    public void hide() {
        representation.delete();
    }

    public void move() {
        if (isTooHigh()) {
            jumping = false;
        }

        if (jumping) {
            if (representation.getY() > jumpTargetHeight) {
                representation.translate(0, -JUMP_SPEED);
            } else {
                jumping = false;
            }
            return;
        }
        representation.translate(0, SPEED);
    }

    public boolean isTooHigh() {
        return representation.getY() <= Game.PADDING;
    }

    public void jump() {
        jumpTargetHeight = representation.getY() - JUMP_HEIGHT;
        jumping = true;
    }

    public void reset() {
        int deltaX = 0;
        int deltaY = (Game.BOTTOM_LIMIT / 2 - representation.getY());
        representation.translate(deltaX, deltaY);
    }

    public int getMaxY() {
        return representation.getMaxY();
    }

    public int getMaxX() {
        return representation.getMaxX() - HITBOX_PADDING;
    }

    public int getX() {
        return representation.getX() + HITBOX_PADDING;
    }

    public int getY() {
        return representation.getY();
    }
}
