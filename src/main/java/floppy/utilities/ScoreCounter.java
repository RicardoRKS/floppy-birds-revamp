package floppy.utilities;

import org.academiadecodigo.simplegraphics.graphics.Text;

public class ScoreCounter {
    private String message = "Your score is: ";
    private int score;
    private Text text;

    public ScoreCounter(int x, int y) {
        score = 0;
        text = new Text(x, y, message + score);
    }

    public void reset() {
        score = 0;
        text.setText(message + score);
    }

    public void increment(int amount) {
        score += amount;
        text.setText(message + score);
    }

    public void show(){
        text.draw();
    }

    public void hide(){
        text.delete();
    }
}
